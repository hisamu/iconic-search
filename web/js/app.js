var $form = $('#search-form'),
    $products = $('#products'),
    $loader = $('#loader');

var productSearchTemplate = Handlebars.compile($('#product-search-template').html()),
    productDetailsTemplate = Handlebars.compile($('#product-details-template').html());

var loadProducts = function(clear) {
    $loader.addClass('active');
    $('#no-results').addClass('hide');

    if (clear) {
        $('#pagination').addClass('hide');
        $products.html('');
        $form.find('#current-page').val(1);
    }

    $.get($form.attr('action'), $form.serialize(), function(data, httpStatus){
        if (data.products === undefined || !data.products.length) {
            $('#pagination').addClass('hide');
            $('#no-results').removeClass('hide');
            $loader.removeClass('active');

            return false;
        }

        $.each(data.products, function(i, product){
            var elem = productSearchTemplate(product);
            $products.append(elem);
        });

        $('#current-page').val(data.page);

        $products.imagesLoaded().progress(function(){
            $products.isotope('layout');

            $('#pagination').toggleClass('hide', !data.more);
            $loader.removeClass('active');
        });
    });
}

$(function(){
    $(document).foundation();

    // Layout setup
    $products.isotope({
        itemSelector: '.product'
    });

    // Product load handlers
    $('.search-button').click(function(e){
        loadProducts(true);
        e.preventDefault();
    });

    $('#pagination > a').click(function(e){
        loadProducts();
        e.preventDefault();
    });

    $form.submit(function(e){
        loadProducts(true);
        e.preventDefault();
    });

    // Link details handler
    $(document).on('click', '#products .product-wrapper', function(e){
        var $this = $(this);

        $loader.addClass('active');
        $('#product-details .details-content').html('');

        $.get(details_url, { id: $this.data('sku') }, function(data){
            if (!data) {
                $loader.removeClass('active');
            }

            $('body').addClass('details-active');
            $('#product-details .details-content').html(productDetailsTemplate(data));

            $loader.removeClass('active');
        });

        e.preventDefault();
    });

    // aside closing
    $('.details-overlay, .details-close').click(function(e){
        $('body').removeClass('details-active');
        e.preventDefault();
    });
});
