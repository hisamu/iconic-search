<?php 

namespace Iconic\Tests\Endpoint;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    protected $client;

    protected function setUp()
    {
        $this->client = new \Iconic\Client();
    }

    /**
     * @test
     */
    public function testClientCreation()
    {
        $this->assertInstanceOf('GuzzleHttp\Client', $this->client->getHttpClient());
    }
}
