<?php 

namespace Iconic\Tests\Endpoint;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class CatalogTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function testProducts()
    {
        $client = new \Iconic\Client(new HttpClient(['handler' => HandlerStack::create(
            new MockHandler([new Response(200, [], json_encode(['_embedded' => ['product' => [['name' => 'Nike Shoes']]]]))])
        )]));

        $response = $client->catalog()->products('nike');
        $products = $response['products'];

        $this->assertEquals($products[0]->getData('name'), 'Nike Shoes');
    }

    /**
     * @test
     */
    public function testNoProducts()
    {
        $client = new \Iconic\Client(new HttpClient(['handler' => HandlerStack::create(
            new MockHandler([new Response(200, [], json_encode(['_embedded' => ['product' => []]]))])
        )]));

        $response = $client->catalog()->products('nike');

        $this->assertEmpty($response['products']);
    }

    /**
     * @test
     */
    public function testPageOverflow()
    {
        $client = new \Iconic\Client(new HttpClient(['handler' => HandlerStack::create(
            new MockHandler([new RequestException('Invalid page provided', new Request('GET', 'test'))])
        )]));

        $response = $client->catalog()->products('abcdef', 9999);

        $this->assertFalse($response);
    }

    /**
     * @test
     */
    public function testSingleProductDetails()
    {
        $client = new \Iconic\Client(new HttpClient(['handler' => HandlerStack::create(
            new MockHandler([new Response(200, [], json_encode([
                'sku' => 'NI126SF58GRH',
                '_embedded' => [
                    'brand' => [
                        'name' => 'Nike'
                    ],
                ],
            ]))])
        )]));

        $response = $client->catalog()->product('NI126SF58GRH');

        $this->assertEquals($response->getData('sku'), 'NI126SF58GRH');
        $this->assertEquals($response->getFirstResource('brand')->getData('name'), 'Nike');
    }
}
