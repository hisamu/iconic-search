ICONIC Search
===
Quick projest specially developed as a test for the Full Stack Developer at The ICONIC. The app features a simple product search over The ICONIC's REST API.

Running the project
---
First, build composer dependencies:
    
    $ composer install

Then, using PHP's internal server:
    
    $ php -S localhost:8080 -t web web/index.php


Backend stack
---
- **Silex**: Simple framework for bootstrapping.
- **Twig**: Template engine. Not really necessary, but a nice extra in case the project needs more views.
- **Guzzle HTTP Client**: Lib for the HTTP part of the API wrapper.
- **Nocarrier\Hal**: A lib for parsing HAL responses. Wasn't really flexible, but I ended up making a fork and improving the project a bit. Pull-request pending :) [[source](https://github.com/blongden/hal)]

Frontend stack
---
- **ZURB Foundation**: Not only a great framework, but also keep the consistency with The ICONIC's stack.
- **jQuery**: Basic usage, AJAX, etc.
- **Handlebars**: Template engine for the product blocks.
- **Isotope / Masonry**: The idea is to get a mosaic layout and the ability to easily filter and order the results in the future.
