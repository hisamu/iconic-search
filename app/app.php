<?php 

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();

$app['debug'] = true;

// Basic services
$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__.'/views',
]);
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// Search API
$app['iconic.api'] = new Iconic\Client();

/**
 * Home
 */
$app->get('/', function() use ($app) {
    return $app['twig']->render('index.html.twig');
});

/**
 * Search API, returns a formatted JSON response
 */
$app->get('/search', function(Request $request) use ($app) {
    $term = $request->get('term');
    $page = $request->get('page');
    
    $response = $app['iconic.api']->catalog()->products($term, $page);

    $result = [
        'products' => [],
        'page' => $page,
        'more' => false,
    ];

    if (!$response) {
        return $app->json($result);
    }

    foreach ($response['products'] as $product) {
        $result['products'][] = [
            'name' => $product->getData('name'),
            'sku' => $product->getData('sku'),
            'price' => $product->getData('price'),
            'brand' => $product->getFirstResource('brand')->getData('name'),
            'image' => $product->getFirstResource('images')->getData('thumbnail'),
            'url' => $product->getData('link'),
        ];
    }

    $result['page'] = ++$page;
    $result['more'] = !!$response['next'];

    return $app->json($result);
})->bind('search');

/**
 * Product details API
 */
$app->get('/product', function(Request $request) use ($app) {
    $response = $app['iconic.api']->catalog()->product($request->get('id'));

    if (!$response) {
        return $app->json([]);
    }
    
    $result = [
        'name' => $response->getData('name'),
        'sku' => $response->getData('sku'),
        'price' => $response->getData('price'),
        'brand' => $response->getFirstResource('brand')->getData('name'),
        'image' => $response->getFirstResource('images')->getData('sprite'),
        'description' => $response->getData('short_description'),
    ];
    
    return $app->json($result);
})->bind('product');

return $app;
