<?php 

namespace Iconic\Endpoint;

use Iconic\Client;
use Nocarrier\Hal;
/**
 * Basic endpoint handling
 */
abstract class AbstractEndpoint
{
    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Transforms a JSON response into a Hal object
     *
     * @param string $response
     */
    public function parse($response)
    {
        return Hal::fromJson($response, 10);
    }
}
