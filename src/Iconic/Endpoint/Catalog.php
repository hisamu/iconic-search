<?php 

namespace Iconic\Endpoint;

use GuzzleHttp\Exception\RequestException;

/**
 * Catalog methods
 */
class Catalog extends AbstractEndpoint
{
    /**
     * Retrieve all the products, filtering by term
     *
     * @param srtring $term
     * @param int $page
     *
     * @return mixed
     */
    public function products($term = null, $page = 1)
    {
        try {
            $response = $this->parse($this->client->get('catalog/products', [
                'q' => $term,
                'page' => $page,
                'page_size' => 10,
            ])->getBody());
        } catch (RequestException $e) {
            return null;
        }

        if (!$response->getResource('product')) {
            return null;
        }

        return [
            'products' => $response->getResource('product'),
            'next' => $response->getLink('next'),
        ];
    }

    /**
     * Fetch the details of a single product on the API.
     * The id can be either the SKU or the desktop URL of the product.
     *
     * @param string $id
     */
    public function product($id)
    {
        try {
            $response = $this->parse($this->client->get(sprintf('catalog/products/%s', $id))->getBody());
        } catch (RequestException $e) {
            return null;
        }

        return $response;
    }
}
