<?php 

namespace Iconic;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Response;
use Iconic\Endpoint\Catalog;

/**
 * Simple ICONIC API Wrapper
 */
class Client
{
    /**
     * Base URL of the API
     *
     * @var string
     */
    protected $baseUrl = 'https://eve.theiconic.com.au/v1/';

    /**
     * Guzzle Client configured instance
     *
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * Creates the client's instance
     *
     * @param null|HttpClient Guzzle Client
     */
    public function __construct(HttpClient $httpClient = null)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Catalog endpoint access
     *
     * @return Catalog
     */
    public function catalog()
    {
        return new Catalog($this);
    }

    /**
     * Performs a GET request
     *
     * @param string $url
     * @param null|array $params
     *
     * @return Response
     */
    public function get($url, $params = null)
    {
        return $this->getHttpClient()->get($url, ['query' => $params]);
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->httpClient ?: $this->createClient();
    }

    /**
     * Creates a new Guzzle Client instance
     */
    protected function createClient()
    {
        return new HttpClient([
            'base_uri' => $this->baseUrl,
            'headers' => ['Accept' => 'application/json'],
        ]);
    }
}
